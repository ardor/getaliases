package nxt.addons;

import nxt.Nxt;
import nxt.NxtException;

import static nxt.http.ParameterParser.getChain;

import nxt.blockchain.Block;
import nxt.blockchain.Chain;
import nxt.db.DbUtils;
import nxt.dbschema.Db;
import nxt.http.APIServlet;
import nxt.http.APITag;
import nxt.util.Convert;

import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.lang.Math;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;

public final class GetAliasesAll implements AddOn {

    public APIServlet.APIRequestHandler getAPIRequestHandler() {
        return new getAliasesAllAPI(new APITag[]{APITag.ADDONS}, "filterForSale", "limit");
    }

    public String getAPIRequestType() {
        return "getAliasesAll";
    }

    public static class getAliasesAllAPI extends APIServlet.APIRequestHandler {

        private getAliasesAllAPI(APITag[] apiTags, String... origParameters) {
            super(apiTags, origParameters);
        }

        @Override
        protected JSONStreamAware processRequest(HttpServletRequest req) throws NxtException {
            Chain chain = getChain(req, true);

            String accountLimit = Convert.emptyToNull(req.getParameter("limit"));
            boolean filterForSale = "true".equalsIgnoreCase(req.getParameter("filterForSale"));

            int varAccountLimit = -1;

            if (accountLimit != null) {
                varAccountLimit = Integer.parseInt(accountLimit);
            }

            Nxt.getBlockchain().readLock();
            Block blockData = Nxt.getBlockchain().getLastBlock();
            LinkedHashMap<String, Long> map = getAlliases(chain, varAccountLimit);
            Nxt.getBlockchain().readUnlock();

            final double floatingPoinmtAdjustment = Math.pow(10, chain.getDecimals());

            JSONArray jsonArray = new JSONArray();

            long balanceNQTTotal = 0;
            int numberOfAccounts = 0;

            for (Map.Entry<String, Long> entry : map.entrySet()) {
                JSONObject json = new JSONObject();

                String alias = entry.getKey();
                long accountId = entry.getValue();

                if(filterForSale) {
                    JO response = nxt.http.callers.GetAliasCall.create().chain(chain.getId()).alias(alias).call();

                    if(!response.isExist("priceNQT")) {
                        continue;
                    }

                    json.put("priceNQT", response.get("priceNQT"));
                    json.put("aliasName", response.get("aliasName"));
                }

                json.put("alias", alias);
                json.put("account", Long.toUnsignedString(accountId));

                numberOfAccounts++;
                jsonArray.add(json);
            }

            JSONObject response = new JSONObject();

            response.put("block", Long.toUnsignedString(blockData.getId()));
            response.put("height", blockData.getHeight());

            response.put("chain", chain.getName());
            response.put("count", numberOfAccounts);

            response.put("aliases", jsonArray);

            return response;
        }

        public static LinkedHashMap<String, Long> getAlliases(Chain chain, final int limit) {
            Connection con = null;

            LinkedHashMap<String, Long> map = new LinkedHashMap<>();

            if(chain.getId() == 1) {
                return map;
            }

            try {
                StringBuilder sb = new StringBuilder();

                con = Db.getConnection();

                sb.append("SELECT account_id,id FROM ");

                sb.append(chain.getName());
                sb.append(".alias");

                sb.append(" WHERE latest = TRUE");

                if(limit > 0) {
                    sb.append(" limit " + limit);
                }

                PreparedStatement pstmt = con.prepareStatement(sb.toString());
                int i = 0;

                ResultSet rs = pstmt.executeQuery();

                while (rs.next()) {
                    long accountId = rs.getLong("ACCOUNT_ID");
                    String alias = rs.getString("ID");

                    map.put(Long.toUnsignedString(Long.valueOf(alias)), accountId);
                };

                con.close();
                return map;

            } catch (SQLException e) {
                DbUtils.close(con);
                throw new RuntimeException(e.toString(), e);
            }
        }

        @Override
        protected boolean isChainSpecific() {
            return true;
        }
    }
}
